/*
  ORIGINAL JAVA CODE:
  Copyright (C) 2014, Jean-Pierre de la Croix
  see the LICENSE file included with this software

  SCALA:
  Copyright (C) 2015, Massimo Maria Ghisalberti (zairik@gmail.com)

 */

package org.pragmas.sphero.heartbeat

trait SpheroHeartbeatListener extends Runnable {

  private var spheroHeartbeat: SpheroHeartbeat = null

  def setHeartbeat(aHeartbeat: SpheroHeartbeat): Unit = {
    spheroHeartbeat = aHeartbeat
  }

  override def run(): Unit = {
    spheroHeartbeat.monitor()
    onChange()
  }

  def onChange(): Unit

}
