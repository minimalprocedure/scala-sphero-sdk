/*
  ORIGINAL JAVA CODE:
  Copyright (C) 2014, Jean-Pierre de la Croix
  see the LICENSE file included with this software

  SCALA:
  Copyright (C) 2015, Massimo Maria Ghisalberti (zairik@gmail.com)

 */

package org.pragmas.sphero.heartbeat

import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import org.pragmas.sphero.util.{ SpheroMessages, SpheroLogger }

class SpheroHeartbeat extends SpheroLogger {

  private val heartbeatLock: Lock = new ReentrantLock()
  private val heartbeatLost: Condition = heartbeatLock.newCondition()

  def monitor(): Unit = {
    heartbeatLock.lock()
    try heartbeatLost.await()
    catch {
      case (e: InterruptedException) => logger.error(SpheroMessages.ERROR_HEARTBEAT_INTERRUPTED)
    } finally heartbeatLock.unlock()
  }

  def kill(): Unit = {
    heartbeatLock.lock()
    try heartbeatLost.signal()
    finally heartbeatLock.unlock()
  }

}

