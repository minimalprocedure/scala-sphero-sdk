/*
  ORIGINAL JAVA CODE:
  Copyright (C) 2014, Jean-Pierre de la Croix
  see the LICENSE file included with this software

  SCALA:
  Copyright (C) 2015, Massimo Maria Ghisalberti (zairik@gmail.com)

 */

package org.pragmas.sphero

import java.io.IOException
import java.util.ArrayList
//import javax.bluetooth.BluetoothStateException
import javax.bluetooth.DeviceClass
import javax.bluetooth.DiscoveryAgent
import javax.bluetooth.DiscoveryListener
import javax.bluetooth.LocalDevice
import javax.bluetooth.RemoteDevice
import javax.bluetooth.ServiceRecord
import javax.bluetooth.UUID
import scala.collection.JavaConversions._
import org.pragmas.sphero.util.{ SpheroLogger, SpheroMessages }

class SpheroDiscovery extends SpheroLogger {

  private val UUIDCLASS = "00001101-0000-1000-8000-00805F9B34FB"

  val spherosDiscovered: ArrayList[Sphero] = new ArrayList[Sphero]()
  private var spheroServiceURL: String = null
  private val inquiryCompletedEvent = new AnyRef()
  private val serviceSearchCompletedEvent = new AnyRef()
  private val discoverylistener = new SpheroDiscoveryListener()

  //private val SPHERO_IEEE_OUI = "000666"
  private val SPHERO_NAME_PREFIX = "SPHERO-"

  def findNearbySpheros(): ArrayList[Sphero] = {

    val localDevice = LocalDevice.getLocalDevice
    val discoveryAgent = localDevice.getDiscoveryAgent
    val serviceUUID = new UUID(UUIDCLASS.replaceAll("-", ""), false)
    val searchUuidSet = Array(serviceUUID)
    val attrIDs: Array[Int] = null

    spherosDiscovered.clear()

    inquiryCompletedEvent.synchronized {
      val isInquiryStarted = discoveryAgent.startInquiry(DiscoveryAgent.GIAC, discoverylistener)
      if (isInquiryStarted) {
        logger.info(SpheroMessages.INFO_BT_INQUIRY_START)
        inquiryCompletedEvent.wait()
        logger.info(SpheroMessages.INFO_BT_INQUIRY_FOUND, spherosDiscovered.size.toString)
      }
    }
    /*
    spherosDiscovered.foreach { sphero =>
      serviceSearchCompletedEvent.synchronized {
        logger.info(SpheroMessages.INFO_BT_SEARCH_SERVICE, sphero.getBluetoothAddress, sphero.friendlyName)
        discoveryAgent.searchServices(attrIDs, searchUuidSet, sphero, discoverylistener)
        serviceSearchCompletedEvent.wait()
      }
      if (spheroServiceURL == null) {
        logger.error(SpheroMessages.ERROR_BT_SEARCH_SERVICE)
      } else {
        sphero.setSpheroUrl(spheroServiceURL)
      }
    }
    */
    spherosDiscovered
  }

  private class SpheroDiscoveryListener extends DiscoveryListener {

    def deviceDiscovered(btDevice: RemoteDevice, btClass: DeviceClass): Unit = {
      /*
      println("---------------------")
      println(btDevice.getBluetoothAddress)
      println(btDevice.getFriendlyName(true))
      println(btDevice.getFriendlyName(false))
      println(btClass.getServiceClasses)
      println("---------------------")
      */
      val friendlyName = btDevice.getFriendlyName(true)
      if (friendlyName.toUpperCase.startsWith(SPHERO_NAME_PREFIX)) {
        //if (btDevice.getBluetoothAddress.startsWith(SPHERO_IEEE_OUI)) {
        logger.info(SpheroMessages.INFO_BT_FOUND)
        var sphero: Sphero = null
        try {
          sphero = new Sphero(btDevice.getBluetoothAddress, friendlyName, Sphero.SPP_DEFAULT_CHANNEL)
        } catch {
          case e: IOException => sphero = new Sphero(btDevice.getBluetoothAddress, s"${SPHERO_NAME_PREFIX}Unknown", Sphero.SPP_DEFAULT_CHANNEL)
        }
        spherosDiscovered.add(sphero)
      }
    }

    def inquiryCompleted(discType: Int): Unit = {
      logger.info(SpheroMessages.INFO_BT_INQUIRY_COMPLETE)
      inquiryCompletedEvent.synchronized {
        inquiryCompletedEvent.notifyAll()
      }
    }

    def servicesDiscovered(transID: Int, serviceRecord: Array[ServiceRecord]): Unit = {
      (0 until serviceRecord.length).foreach { i =>
        val url = serviceRecord(i).getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false)
        if (url == null) { //continue }
          spheroServiceURL = new String(url)
          logger.info(SpheroMessages.INFO_BT_FOUND_SERVICE, url)
          //break
        }
      }
    }

    def serviceSearchCompleted(transID: Int, respCode: Int): Unit = {
      logger.info(SpheroMessages.INFO_BT_SEARCH_SERVICE_COMPLETE)
      serviceSearchCompletedEvent.synchronized {
        serviceSearchCompletedEvent.notifyAll()
      }
    }
  }
}

