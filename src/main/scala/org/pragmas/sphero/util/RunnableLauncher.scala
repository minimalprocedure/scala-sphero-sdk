/*
  ORIGINAL JAVA CODE:
  Copyright (C) 2014, Jean-Pierre de la Croix
  see the LICENSE file included with this software

  SCALA:
  Copyright (C) 2015, Massimo Maria Ghisalberti (zairik@gmail.com)

 */

package org.pragmas.sphero.util

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class RunnableLauncher extends SpheroLogger {
  private val runnableService: ExecutorService = Executors.newSingleThreadExecutor()

  def launch(aRunnable: Runnable): Unit = {
    runnableService.submit(aRunnable)
    runnableService.shutdown()
    try
      while (!runnableService.awaitTermination(60, TimeUnit.SECONDS)) {
        logger.info(SpheroMessages.INFO_RUNNABLE_TO_STOP)
      }
    catch {
      case (e: InterruptedException) =>
        logger.error(SpheroMessages.ERROR_RUNNABLE_TO_STOP)
    }
  }
}
