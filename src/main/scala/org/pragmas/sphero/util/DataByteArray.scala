/*
  ORIGINAL JAVA CODE:
  Copyright (C) 2014, Jean-Pierre de la Croix
  see the LICENSE file included with this software

  SCALA:
  Copyright (C) 2015, Massimo Maria Ghisalberti (zairik@gmail.com)

 */

package org.pragmas.sphero.util

import java.io.ByteArrayOutputStream

object Utils {

  def baToString(ba: Array[Byte]): String = {
    val s = new StringBuilder()
    s.append("[ ")
    ba.foreach { i => s.append("%02Xh ".format(i & 0xff)) }
    s.append("]")
    s.toString
  }

}

class DataByteArray extends ByteArrayOutputStream {

  def writeFourBytes(anInt: Int): Unit = {
    write(anInt >> 24)
    write(anInt >> 16)
    write(anInt >> 8)
    write(anInt)
  }

  def writeTwoBytes(aShort: Int): Unit = {
    write(aShort >> 8)
    write(aShort)
  }

  def writeOneByte(aByte: Int): Unit = {
    write(aByte)
  }

  def writeBytes(bytes: Int, nBytes: Int): Unit = {
    var currBytes = nBytes
    if (currBytes > 0 && currBytes <= 4) {
      while (currBytes > 0) {
        write(bytes >> 8 * (currBytes - 1))
        currBytes = currBytes - 1
      }
    }
  }
}
