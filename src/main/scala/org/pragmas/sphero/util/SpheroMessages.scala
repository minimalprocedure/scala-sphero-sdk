package org.pragmas.sphero.util

import com.typesafe.scalalogging.LazyLogging

object SpheroMessages {

  val START = "Starting Sphero..."
  val DONE = "Sphero done."

  val SET_RGB_LED = "Sphero command SET_RGB_LED data ({})"
  val SET_BACK_LED = "Sphero command SET_BACK_LED data ({})"
  val SET_CAL = "Sphero command SET_CAL data ({})"
  val SET_DATA_STREAMING = "Sphero command SET_DATA_STREAMING data ({})"
  val UNSET_DATA_STREAMING = "Sphero command (UN)SET_DATA_STREAMING data ({})"
  val SET_STABILIZ = "Sphero command SET_STABILIZ data ({})"
  val ROLL = "Sphero command ROLL data ({})"
  val SLEEP = "Sphero command SLEEP data ({})"
  val PING = "Sphero command PING data ({})"
  val PING_RESPONSE = "Sphero command PING response ({})"
  val VERSION = "Sphero command VERSION data ({})"
  val VERSION_RESPONSE = "Sphero command VERSION response ({})"

  val SIMPLE_RESPONSE = "Sphero response ({})"

  val CONNECTION_AT_URL = "Sphero ({}) connection at {}"
  val DISCONNECTION_AT_URL = "Sphero ({}) disconnection at {}"

  val SENDING_COMMAND = "Sending command to Sphero ({}) packets ({})"
  val COMMAND_PACKETS = "Sending {} packets ({})"

  val ERROR_SERVICE_URL = "New Sphero service URL is not properly formatted."
  val ERROR_SERVICE_URL_CHANGE = "Unable to change Sphero ({}) URL while connected."
  val ERROR_CONNECTION_AT_URL = "Unable to connect to Sphero ({}) at {}"
  val ERROR_DISCONNECTION_AT_URL = "Unable to disconnect Sphero ({}) at {}"
  val ERROR_CONNECTION = "Unable to connect to Sphero ({})"
  val ERROR_CONNECTED = "Sphero ({}) is not connected!"
  val ERROR_DISCONNECTED = "Sphero ({}) is not connected!"
  val ERROR_ALREADY_CONNECTED = "Sphero ({}) is already connected."
  val ERROR_ALREADY_DISCONNECTED = "Sphero ({}) is already disconnected."

  val ERROR_HEARTBEAT_INTERRUPTED = "Monitoring heartbeat was interrupted."
  val ERROR_RUNNABLE_TO_STOP = "Interrupted while waiting for Runnable to finish."

  val ERROR_SENDING_COMMAND_TO_QUEUE = "Sending command to the queue was interrupted."
  val ERROR_RECEIVING_RESPONSE_FROM_QUEUE = "Receiving response from the queue was interrupted."
  val ERROR_SENDING_COMMAND = "Unable to write command to Sphero ({})"
  val ERROR_SENDING_COMMAND_INTERRUPTED = "Unable to write command to Sphero ({}) - interrupted"

  val ERROR_RECEIVING_COMMAND = "Unable to read response from Sphero ({})"
  val ERROR_RECEIVING_COMMAND_INTERRUPTED = "Unable to read response from Sphero ({}) - interrupted"

  val ERROR_INTEGER_RANGE = "Expected integer {} in range [{},{}]."
  val ERROR_HEXADECIMAL_PARSE = "Unable to parse hexadecimal color for RGB LED. Expected, for example, ff1493."
  val ERROR_VALID_OPTIONS = "Expected a valid set of options."

  val ERROR_SAMPLING_RATE_FACTOR = "Sampling rate factor has to be greater than zero."
  val ERROR_FRAMES_PER_PACKETS = "Frames per packet has to be greater than zero."
  val ERROR_PACKETS_ZERO_VALUE = "Packets per stream has to be zero for unlimited or greater than zero."

  val ERROR_CHECKSUM = "Invalid checksum detected {} vs {}"

  val INFO_NO_PACKETS_IN_QUEUE = "No packets from Sphero ({}) are currently in the queue."
  val INFO_DATA_STREAM_CONTINUE = "Data stream from Sphero will continue until it is disabled."
  val INFO_DATA_STREAM_ENDING = "Data stream from Sphero will end after {} packet(s)."
  val INFO_RUNNABLE_TO_STOP = "Waiting for Runnable to finish..."

  val INFO_BT_INQUIRY_START = "Waiting for inquiry to complete..."
  val INFO_BT_INQUIRY_COMPLETE = "Inquiry for any nearby Spheros completed!"
  val INFO_BT_INQUIRY_FOUND = "Found {} Sphero(s)!"
  val INFO_BT_SEARCH_SERVICE = "Searching services on {} {}..."
  val INFO_BT_FOUND_SERVICE = "Found service (RN-SPP) at {}"
  val INFO_BT_SEARCH_SERVICE_COMPLETE = "Service search on Sphero completed!"
  val ERROR_BT_SEARCH_SERVICE = "No services found. Using default Sphero service URL."
  val INFO_BT_FOUND = "Found a Sphero nearby!"

}

trait SpheroLogger extends LazyLogging {
}
