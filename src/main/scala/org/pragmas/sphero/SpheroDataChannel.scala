/*
  ORIGINAL JAVA CODE:
  Copyright (C) 2014, Jean-Pierre de la Croix
  see the LICENSE file included with this software

  SCALA:
  Copyright (C) 2015, Massimo Maria Ghisalberti (zairik@gmail.com)

 */

package org.pragmas.sphero

import java.io.ByteArrayOutputStream
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException
import java.util.ArrayList
import java.util.concurrent.BlockingQueue

import java.util.concurrent.Executors
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeUnit

import javax.microedition.io.StreamConnection
import org.pragmas.sphero.heartbeat.SpheroHeartbeat
import org.pragmas.sphero.packets.SpheroAsynchronousPacket
import org.pragmas.sphero.packets.SpheroCommandPacket
import org.pragmas.sphero.packets.SpheroPacket
import org.pragmas.sphero.packets.SpheroResponsePacket
import org.pragmas.sphero.util.{ Utils, SpheroMessages, SpheroLogger }

class SpheroDataChannel(spheroConnection: StreamConnection, spheroFriendlyName: String, spheroHeartbeat: SpheroHeartbeat) extends SpheroLogger {

  private var spheroCommandLine: DataOutputStream = null
  private var spheroResponseLine: DataInputStream = null
  private val incomingQueue: BlockingQueue[SpheroResponsePacket] = new LinkedBlockingQueue()
  private val outgoingQueue: BlockingQueue[SpheroCommandPacket] = new LinkedBlockingQueue()
  private val inboundProcessor = new InboundProcessor()
  private val outboundProcessor = new OutboundProcessor()
  private val processingService = Executors.newFixedThreadPool(2)

  def send(packet: SpheroCommandPacket, commandName: String = ""): Unit = {
    logger.info(SpheroMessages.COMMAND_PACKETS, commandName, packet.toString)
    try {
      outgoingQueue.put(packet)
    } catch {
      case e: InterruptedException =>
        logger.error(SpheroMessages.ERROR_SENDING_COMMAND_TO_QUEUE)
    }
  }

  def receive(): SpheroResponsePacket = {
    var response: SpheroResponsePacket = null
    try {
      while (response == null && inboundProcessor.isRunning) {
        response = incomingQueue.poll(1, TimeUnit.SECONDS)
      }
    } catch {
      case e: InterruptedException =>
        logger.error(SpheroMessages.ERROR_RECEIVING_RESPONSE_FROM_QUEUE)
    }
    logger.info(SpheroMessages.SIMPLE_RESPONSE, Utils.baToString(response.toByteArray))
    response
  }

  def receiveAll(): ArrayList[SpheroResponsePacket] = {
    val allQueuedResponses = new ArrayList[SpheroResponsePacket]()
    incomingQueue.drainTo(allQueuedResponses)
    allQueuedResponses
  }

  def numberOfQueuedResponses(): Int = incomingQueue.size

  def open(): Unit = {
    spheroCommandLine = spheroConnection.openDataOutputStream()
    spheroResponseLine = spheroConnection.openDataInputStream()
    if (!processingService.isShutdown) {
      processingService.execute(inboundProcessor)
      processingService.execute(outboundProcessor)
    }
  }

  def close(): Unit = {
    processingService.shutdownNow()
    spheroCommandLine.close()
    spheroResponseLine.close()
  }

  private class OutboundProcessor extends Runnable {

    var isRunning: Boolean = false

    override def run(): Unit = {
      isRunning = true
      try {
        while (!processingService.isShutdown) {
          val packetArray = outgoingQueue.take.toByteArray
          spheroCommandLine.write(packetArray)
        }
      } catch {
        case e: InterruptedException =>
          logger.error(SpheroMessages.ERROR_SENDING_COMMAND_INTERRUPTED, spheroFriendlyName)
        case e: IOException =>
          logger.error(SpheroMessages.ERROR_SENDING_COMMAND, spheroFriendlyName)
      } finally {
        spheroHeartbeat.kill()
        isRunning = false
      }
    }
  }

  private class InboundProcessor extends Runnable {

    var isRunning: Boolean = false

    private def defaultAction(incomingByteStream: ByteArrayOutputStream, spheroResponseLine: DataInputStream): Byte = {
      incomingByteStream.write(spheroResponseLine.readByte().toInt) // MSRP
      incomingByteStream.write(spheroResponseLine.readByte().toInt) // SEQ
      val DLEN = spheroResponseLine.readByte()
      incomingByteStream.write(DLEN.toInt) // DLEN
      DLEN
    }

    private def asyncAction(incomingByteStream: ByteArrayOutputStream, spheroResponseLine: DataInputStream): Byte = {
      incomingByteStream.write(spheroResponseLine.readByte().toInt) // ID CODE
      val DLEN_MSB = spheroResponseLine.readByte()
      incomingByteStream.write(DLEN_MSB.toInt) // DLEN MSB
      val DLEN_LSB = spheroResponseLine.readByte()
      incomingByteStream.write(DLEN_LSB.toInt) // DLEN LSB
      ((DLEN_MSB << 8) | (DLEN_LSB)).toByte
    }

    override def run(): Unit = {
      isRunning = true
      try {
        while (!processingService.isShutdown) {
          var lengthOfData = 0
          var isAsynchronous = false

          val incomingByteStream = new ByteArrayOutputStream()
          incomingByteStream.write(spheroResponseLine.readByte().toInt) // SOP1
          val SOP2 = spheroResponseLine.readByte()
          incomingByteStream.write(SOP2.toInt) // SOP2

          val defByteCode = SpheroPacket.SOP.DEFAULT.byteCode
          val asyncByteCode = SpheroPacket.SOP.ASYNC.byteCode

          lengthOfData = (SOP2 match {
            case `defByteCode` =>
              isAsynchronous = false
              defaultAction(incomingByteStream, spheroResponseLine)
            case `asyncByteCode` =>
              isAsynchronous = true
              asyncAction(incomingByteStream, spheroResponseLine)
            case _ =>
              isAsynchronous = false
              defaultAction(incomingByteStream, spheroResponseLine)
          }).toInt

          (0 until (lengthOfData - 1)).foreach(i => {
            incomingByteStream.write(spheroResponseLine.readByte().toInt) // DATA
          })
          incomingByteStream.write(spheroResponseLine.readByte().toInt) // CHK

          val packet = if (isAsynchronous)
            new SpheroAsynchronousPacket(incomingByteStream.toByteArray())
          else
            new SpheroResponsePacket(incomingByteStream.toByteArray())

          incomingQueue.put(packet)
        }
      } catch {
        case e: IOException =>
          logger.error(SpheroMessages.ERROR_RECEIVING_COMMAND, spheroFriendlyName)
        case e: InterruptedException =>
          logger.error(SpheroMessages.ERROR_RECEIVING_COMMAND_INTERRUPTED, spheroFriendlyName)
      } finally {
        spheroHeartbeat.kill()
        isRunning = false
      }
    }
  }
}
