/*
  ORIGINAL JAVA CODE:
  Copyright (C) 2014, Jean-Pierre de la Croix
  see the LICENSE file included with this software

  SCALA:
  Copyright (C) 2015, Massimo Maria Ghisalberti (zairik@gmail.com)

 */

package org.pragmas.sphero

import org.pragmas.sphero.packets.{ SpheroResponsePacket, SpheroAsynchronousPacket, SpheroDataStreamingOptions }

object SpheroApplication {

  def main2(args: Array[String]): Unit = {
    val discovery = new SpheroDiscovery()
    discovery.findNearbySpheros()
  }

  def main(args: Array[String]): Unit = {
    val s = new SpheroRunner()
    s.start { sphero =>

      val colorSequence = Array(
        sphero.colors.FIREBRICK,
        sphero.colors.GREEN,
        sphero.colors.PURPLE,
        sphero.colors.SIENNA,
        sphero.colors.YELLOW,
        sphero.colors.THISTLE,
        sphero.colors.BROWN
      )
      colorSequence.foreach(color => {
        sphero.color(color, true)
        try Thread.sleep(700)
        catch {
          case e: InterruptedException => e.printStackTrace()
        }
      })

      //Thread.sleep(500)
      //sphero.ping()

      Thread.sleep(500)

      sphero.setStabilizationMode(true)
      sphero.roll(400, 0, 1)
      Thread.sleep(1000)
      sphero.roll(400, 90, 1)
      Thread.sleep(1000)
      sphero.roll(400, 0, 1)
      Thread.sleep(1000)

      sphero.sleepAndDisconnect()

      //sphero.setStabilizationMode(true)
      //sphero.setBackLedBrightness(0)

      //sphero.sleep(0)

      while (sphero.isConnected) {
        val packet = sphero.waitForNextPacket()
        if (packet.isAsynchronous) {
          println("isAsynchronous")
          println(packet)
          val data = packet.asInstanceOf[SpheroAsynchronousPacket].parseDataWithOptions(sphero.options)
          data.keys.foreach { key =>
            println(key + " : " + data(key))
          }
        } else {
          println("isSynchronous")
          val data = packet.asInstanceOf[SpheroResponsePacket]
          println(data)
        }
      }

    }

  }
}
