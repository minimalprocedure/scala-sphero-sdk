/*
  ORIGINAL JAVA CODE:
  Copyright (C) 2014, Jean-Pierre de la Croix
  see the LICENSE file included with this software

  SCALA:
  Copyright (C) 2015, Massimo Maria Ghisalberti (zairik@gmail.com)

 */

package org.pragmas.sphero

import java.util.ArrayList
import org.pragmas.sphero.packets.{ SpheroResponsePacket, SpheroAsynchronousPacket, SpheroDataStreamingOptions }
import org.pragmas.sphero.util.RunnableLauncher
import org.pragmas.sphero.util.{ SpheroLogger, SpheroMessages }
import scala.collection.JavaConversions._

class SpheroBasicRunnable(aSphero: Sphero, callback: Function[Sphero, Unit]) extends Runnable {

  override def run(): Unit = {
    callback(aSphero)
  }
}

class SpheroRunner() extends SpheroLogger {

  //private var addr: String = null
  //private var name: String = null
  //private var sppChannel: Int = Sphero.SPP_DEFAULT_CHANNEL

  //var options: SpheroDataStreamingOptions = null
  private val spheroDiscovery: SpheroDiscovery = new SpheroDiscovery()
  var spherosDiscovered: ArrayList[Sphero] = spheroDiscovery.findNearbySpheros()

  val sphero: Sphero =
    if (spherosDiscovered.size > 0) {
      spherosDiscovered.get(0)
    } else null

  def start(spheroFun: Sphero => Unit): Unit = {
    logger.info(SpheroMessages.START)
    if (sphero != null) {

      sphero.options = new SpheroDataStreamingOptions(200, 1, 0)
      sphero.options.addOptions(
        SpheroDataStreamingOptions.MASK.IMU_PITCH_ANGLE_FILTERED,
        SpheroDataStreamingOptions.MASK.IMU_ROLL_ANGLE_FILTERED,
        SpheroDataStreamingOptions.MASK.IMU_YAW_ANGLE_FILTERED
      )
      sphero.options.addOptions2(
        SpheroDataStreamingOptions.MASK2.QUARTERNION_Q0,
        SpheroDataStreamingOptions.MASK2.QUARTERNION_Q1,
        SpheroDataStreamingOptions.MASK2.QUARTERNION_Q2,
        SpheroDataStreamingOptions.MASK2.QUARTERNION_Q3
      )

      sphero.connect()
      sphero.enableDataStreaming(sphero.options)

      (new RunnableLauncher()).launch(
        new SpheroBasicRunnable(sphero, spheroFun)
      )

      //sphero.disableDataStreaming()
      if (!sphero.isConnected)
        sphero.disconnect()
    }
    logger.info(SpheroMessages.DONE)
  }

}
