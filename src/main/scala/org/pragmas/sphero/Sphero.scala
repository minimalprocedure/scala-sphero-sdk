/*
  ORIGINAL JAVA CODE:
  Copyright (C) 2014, Jean-Pierre de la Croix
  see the LICENSE file included with this software

  SCALA:
  Copyright (C) 2015, Massimo Maria Ghisalberti (zairik@gmail.com)

 */

package org.pragmas.sphero

import java.io.IOException
import java.util.ArrayList
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import javax.bluetooth.RemoteDevice
import javax.microedition.io.Connector
import javax.microedition.io.StreamConnection
import org.pragmas.sphero.heartbeat.SpheroHeartbeat
import org.pragmas.sphero.heartbeat.SpheroHeartbeatListener
import org.pragmas.sphero.packets.{
  SpheroCommandPacket,
  SpheroDataStreamingOptions,
  SpheroPacket,
  SpheroResponsePacket
}
import org.pragmas.sphero.util.{ Utils, DataByteArray, SpheroLogger, SpheroMessages, SpheroColors }
import collection.JavaConversions._

object Sphero {
  val SPP_DEFAULT_CHANNEL = 1
}

class Sphero(addr: String, name: String, sppChannel: Int) extends RemoteDevice(addr.replaceAll(":", "")) with SpheroLogger {

  val colors = org.pragmas.sphero.util.SpheroColors

  private val address = addr.replaceAll(":", "")
  private var spheroURL: String = s"btspp://$address:$sppChannel;authenticate=false;encrypt=false;master=false"
  val friendlyName: String = name

  private var spheroConnection: StreamConnection = null
  private var spheroDataChannel: SpheroDataChannel = null
  private var _isConnected: Boolean = false
  private var heartbeatService: ExecutorService = _
  private val spheroHeartbeat: SpheroHeartbeat = new SpheroHeartbeat()

  var options: SpheroDataStreamingOptions = null

  def setSpheroUrl(url: String): Unit = {
    if (!isConnected) {
      if (url.matches(s"btspp://${this.getBluetoothAddress}:([1-9]|[1-3]?[0-9]);authenticate=(true|false);encrypt=(true|false);master=(true|false)")) {
        spheroURL = new String(url)
      } else {
        logger.error(SpheroMessages.ERROR_SERVICE_URL)
      }
    } else {
      logger.error(SpheroMessages.ERROR_SERVICE_URL_CHANGE, friendlyName)
    }
  }

  def connect(): Unit = {
    if (!isConnected) {
      try {
        startHeartbeat()
        spheroConnection = Connector.open(spheroURL).asInstanceOf[StreamConnection]
        spheroDataChannel = new SpheroDataChannel(spheroConnection, friendlyName, spheroHeartbeat)
        spheroDataChannel.open()
        _isConnected = true
        logger.info(SpheroMessages.CONNECTION_AT_URL, friendlyName, spheroURL)
      } catch {
        case e: IOException =>
          logger.error(SpheroMessages.ERROR_CONNECTION_AT_URL, friendlyName, spheroURL)
      }
    } else logger.error(SpheroMessages.ERROR_ALREADY_CONNECTED, friendlyName)
  }

  def disconnect(): Unit = {
    if (isConnected) {
      try {
        spheroDataChannel.close()
        spheroConnection.close()
        _isConnected = false
        stopHeartbeat()
        logger.info(SpheroMessages.DISCONNECTION_AT_URL, friendlyName, spheroURL)
      } catch {
        case e: IOException =>
          logger.error(SpheroMessages.ERROR_DISCONNECTION_AT_URL, friendlyName, spheroURL)
      }
    } else logger.error(SpheroMessages.ERROR_ALREADY_DISCONNECTED, friendlyName)
  }

  def isConnected: Boolean = {
    if (!_isConnected) {
      logger.error(SpheroMessages.ERROR_CONNECTED, friendlyName)
    }
    _isConnected
  }

  private def startHeartbeat(): Unit = {
    heartbeatService = Executors.newSingleThreadExecutor()
    this.addHeartbeatListener(new SpheroHeartbeatListener() {
      override def onChange(): Unit = { disconnect() }
    })
  }

  private def stopHeartbeat(): Unit = {
    heartbeatService.shutdown()
  }

  private def addHeartbeatListener(aListener: SpheroHeartbeatListener): Unit = {
    aListener.setHeartbeat(spheroHeartbeat)
    heartbeatService.execute(aListener)
  }

  def ping(): Unit = {
    if (isConnected) {
      val data = Array[Byte]()
      spheroDataChannel.send(new SpheroCommandPacket(SpheroPacket.DID.CORE, SpheroPacket.CID.PING,
        null, 0x01), "PING")

      val responseData = spheroDataChannel.receive()
      //println(responseData.size)

      //responseData.foreach { rsp => logger.error(SpheroMessages.PING_RESPONSE, rsp.toByteArray.mkString(" ")) }

      //logger.info(SpheroMessages.PING_RESPONSE, Utils.baToString(responseData.toByteArray))

    } else logger.error(SpheroMessages.ERROR_DISCONNECTED, friendlyName)
  }

  def version(): Unit = {
    if (isConnected) {
      val data = Array[Byte]()
      spheroDataChannel.send(new SpheroCommandPacket(SpheroPacket.DID.CORE, SpheroPacket.CID.VERSION,
        null, data.length))
      logger.info(SpheroMessages.VERSION, Utils.baToString(data))

      val responseData = spheroDataChannel.receive()
    } else logger.error(SpheroMessages.ERROR_DISCONNECTED, friendlyName)
  }
  def setDeviceName(name: String): Unit = {}
  def getBluetoothInfo(): Unit = {}
  def setAutoReconnect(flag: Int, time: Int): Unit = {}
  def getAutoReconnect(): Unit = {}
  def getPowerState(): Unit = {}
  def setPowerNotification(flag: Int): Unit = {}

  def sleep(wakeup: Int = 0, m: Int = 0, orbBasic: Int = 0): Unit = {
    if (isConnected) {
      val data = Array((wakeup >> 8).toByte, wakeup.toByte, m.toByte, (orbBasic >> 8).toByte, orbBasic.toByte)
      spheroDataChannel.send(
        new SpheroCommandPacket(SpheroPacket.DID.CORE, SpheroPacket.CID.SLEEP, data, data.length),
        "SLEEP"
      )
    }
  }

  def sleepAndDisconnect(): Unit = {
    sleep()
    disconnect()
  }

  def getVoltageTripPoints(): Unit = {}
  def setInactivityTimeout(time: Int): Unit = {}
  def runL1Diags(): Unit = {}
  def runL2Diags(): Unit = {}
  def assignTime(time: Int): Unit = {}
  def pollPacketTimes(time: Int): Unit = {}

  def randomColor(): Unit = {}
  def getColor(): Unit = {}
  def detectCollisions(): Unit = {}
  def startCalibration(): Unit = {}
  def finishCalibration(): Unit = {}
  def streamOdometer(sps: Int = 5, remove: Boolean = false): Unit = {}
  def streamVelocity(sps: Int = 5, remove: Boolean = false): Unit = {}
  def streamAccelOne(sps: Int = 5, remove: Boolean = false): Unit = {}
  def streamImuAngles(sps: Int = 5, remove: Boolean = false): Unit = {}
  def streamAccelerometer(sps: Int = 5, remove: Boolean = false): Unit = {}
  def streamGyroscope(sps: Int = 5, remove: Boolean = false): Unit = {}
  def streamMotorsBackEmf(sps: Int = 5, remove: Boolean = false): Unit = {}
  def stopOnDisconnect(remove: Boolean = false): Unit = {}
  def stop(): Unit = {}

  def roll(speed: Int, heading: Int, state: Int): Unit = {
    if (isConnected) {
      if (heading >= 0 && heading <= 359) {
        val data = Array(speed.toByte, (heading >> 8).toByte, heading.toByte, state.toByte)
        spheroDataChannel.send(new SpheroCommandPacket(SpheroPacket.DID.SPHERO, SpheroPacket.CID.ROLL,
          data, data.length))
        logger.info(SpheroMessages.ROLL, Utils.baToString(data))
      } else logger.error(SpheroMessages.ERROR_INTEGER_RANGE, "heading", "0", "359")
    } else logger.error(SpheroMessages.ERROR_DISCONNECTED, friendlyName)
  }

  def setRgbLedColor(hexColor: String, isPersistant: Boolean) {
    if (isConnected) {
      if (hexColor != null && hexColor.matches("[0-9A-Fa-f]{6}")) {
        val r = java.lang.Integer.parseInt(hexColor.substring(0, 2), 16).toByte
        val g = java.lang.Integer.parseInt(hexColor.substring(2, 4), 16).toByte
        val b = java.lang.Integer.parseInt(hexColor.substring(4, 6), 16).toByte
        setRgbLedColor(r, g, b, isPersistant)
      } else logger.error(SpheroMessages.ERROR_HEXADECIMAL_PARSE)
    } else logger.error(SpheroMessages.ERROR_DISCONNECTED, friendlyName)
  }

  def color(color: SpheroColors.COLOR, isPersistant: Boolean = false): Unit = {
    setRgbLedColor(color.rgb(0), color.rgb(0), color.rgb(0), isPersistant)
  }

  def setRgbLedColor(hexColor: String): Unit = {
    setRgbLedColor(hexColor, false)
  }

  def setRgbLedColor(
    r: Byte,
    g: Byte,
    b: Byte,
    isPersistant: Boolean
  ): Unit = {
    if (isConnected) {
      val data = Array(r, g, b, (if (isPersistant) 1 else 0).toByte)
      spheroDataChannel.send(new SpheroCommandPacket(SpheroPacket.SOP.DEFAULT, SpheroPacket.DID.SPHERO,
        SpheroPacket.CID.SET_RGB_LED, data, data.length), "SET_RGB_LED")

      val responseData = spheroDataChannel.receive()
    } else logger.error(SpheroMessages.ERROR_DISCONNECTED, friendlyName)
  }

  def setRgbLedColor(r: Byte, g: Byte, b: Byte): Unit = {
    setRgbLedColor(r, g, b, false)
  }

  def setBackLedBrightness(brightness: Int): Unit = {
    if (isConnected) {
      if (brightness >= 0 && brightness <= 255) {
        val data = Array(brightness.toByte)
        spheroDataChannel.send(new SpheroCommandPacket(SpheroPacket.DID.SPHERO, SpheroPacket.CID.SET_BACK_LED,
          data, data.length), "SET_BACK_LED")
      } else logger.error(SpheroMessages.ERROR_INTEGER_RANGE, "brightness", "0", "255")
    } else logger.error(SpheroMessages.ERROR_DISCONNECTED, friendlyName)
  }

  def setRelativeHeading(heading: Int): Unit = {
    if (isConnected) {
      if (heading >= SpheroPacket.CID.SET_CAL_MIN && heading <= SpheroPacket.CID.SET_CAL_MAX) {
        //val data = Array[Byte]((heading >> 8).toByte, heading.toByte)
        val byteA = new DataByteArray()
        byteA.writeTwoBytes(heading)
        val data = byteA.toByteArray
        spheroDataChannel.send(new SpheroCommandPacket(SpheroPacket.DID.SPHERO, SpheroPacket.CID.SET_CAL,
          data, data.length), "SET_CAL")
      } else logger.error(SpheroMessages.ERROR_INTEGER_RANGE, "heading", "0", "359")
    } else logger.error(SpheroMessages.ERROR_DISCONNECTED, friendlyName)
  }

  def enableDataStreaming(options: SpheroDataStreamingOptions): Unit = {
    if (isConnected) {
      if (options != null) {
        val data = options.toByteArray
        spheroDataChannel.send(new SpheroCommandPacket(SpheroPacket.DID.SPHERO, SpheroPacket.CID.SET_DATA_STREAMING,
          data, data.length), "SET_DATA_STREAMING")
      } else logger.error(SpheroMessages.ERROR_VALID_OPTIONS)
    } else logger.error(SpheroMessages.ERROR_DISCONNECTED, friendlyName)
  }

  def disableDataStreaming(): Unit = {
    if (isConnected) {
      val options = new SpheroDataStreamingOptions()
      options.addOptions(SpheroDataStreamingOptions.MASK.DISABLE)
      val data = options.toByteArray
      spheroDataChannel.send(new SpheroCommandPacket(SpheroPacket.DID.SPHERO, SpheroPacket.CID.SET_DATA_STREAMING,
        data, data.length), "(UN)SET_DATA_STREAMING")
    } else logger.error(SpheroMessages.ERROR_DISCONNECTED, friendlyName)
  }

  def setStabilizationMode(isStabilizationOn: Boolean): Unit = {
    if (isConnected) {
      val data = if (isStabilizationOn)
        Array[Byte](SpheroPacket.CID.SET_STABILIZ_ON)
      else
        Array[Byte](SpheroPacket.CID.SET_STABILIZ_OFF)
      spheroDataChannel.send(new SpheroCommandPacket(SpheroPacket.DID.SPHERO, SpheroPacket.CID.SET_STABILIZ,
        data, data.length), "SET_STABILIZ")
    } else logger.error(SpheroMessages.ERROR_DISCONNECTED, friendlyName)
  }

  def getNextPacket: SpheroResponsePacket = {
    if (spheroDataChannel.numberOfQueuedResponses > 0) {
      spheroDataChannel.receive()
    } else {
      logger.info(SpheroMessages.INFO_NO_PACKETS_IN_QUEUE, friendlyName)
      null
    }
  }

  def waitForNextPacket(): SpheroResponsePacket = spheroDataChannel.receive()

  def getAllPackets: ArrayList[SpheroResponsePacket] = spheroDataChannel.receiveAll()
}

