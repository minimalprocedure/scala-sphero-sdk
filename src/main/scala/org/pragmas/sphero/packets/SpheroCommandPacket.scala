/*
  ORIGINAL JAVA CODE:
  Copyright (C) 2014, Jean-Pierre de la Croix
  see the LICENSE file included with this software

  SCALA:
  Copyright (C) 2015, Massimo Maria Ghisalberti (zairik@gmail.com)

 */

package org.pragmas.sphero.packets

import java.io.ByteArrayOutputStream

object SpheroCommandPacket {
  private var sequenceNumber: Byte = 0
}

class SpheroCommandPacket(deliveryOption: SpheroPacket.SOP.SOP, deviceID: SpheroPacket.DID.DID, commandID: SpheroPacket.CID.CID,
    data: Array[Byte], length: Int) extends SpheroPacket {

  val byteStream = new ByteArrayOutputStream()
  byteStream.write(SpheroPacket.SOP.DEFAULT.byteCode.toInt) // SOP1
  byteStream.write(deliveryOption.byteCode.toInt) // SOP2
  isAsynchronous =
    if (deliveryOption == SpheroPacket.SOP.ASYNC || deliveryOption == SpheroPacket.SOP.NORESET_ASYNC)
      true
    else false
  byteStream.write(deviceID.byteCode.toInt) // DID
  byteStream.write(commandID.byteCode.toInt) // CID
  SpheroCommandPacket.sequenceNumber.+(1)
  byteStream.write(SpheroCommandPacket.sequenceNumber.toInt) // sequence number
  byteStream.write(length + 1) // length of data and checksum in bytes
  if (data != null)
    byteStream.write(data, 0, length) // data
  byteStream.write(computeChecksum(byteStream.toByteArray, byteStream.size + 1).toInt) // checksum

  internalByteArray = byteStream.toByteArray

  def this(
    deviceID: SpheroPacket.DID.DID,
    commandID: SpheroPacket.CID.CID,
    data: Array[Byte],
    length: Int
  ) {
    this(SpheroPacket.SOP.DEFAULT, deviceID, commandID, data, length)
  }
}

