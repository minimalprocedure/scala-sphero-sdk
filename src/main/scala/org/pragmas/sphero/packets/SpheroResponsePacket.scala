/*
  ORIGINAL JAVA CODE:
  Copyright (C) 2014, Jean-Pierre de la Croix
  see the LICENSE file included with this software

  SCALA:
  Copyright (C) 2015, Massimo Maria Ghisalberti (zairik@gmail.com)

 */

package org.pragmas.sphero.packets

import org.pragmas.sphero.util.{ SpheroLogger, SpheroMessages }

class SpheroResponsePacket(byteArray: Array[Byte]) extends SpheroPacket with SpheroLogger {
  val checksum = computeChecksum(byteArray, byteArray.length)
  internalByteArray = byteArray.clone()
  if (!(checksum == byteArray(byteArray.length - 1))) {
    logger.error(SpheroMessages.ERROR_CHECKSUM, "%02X".format(checksum), "%02X".format(byteArray(byteArray.length - 1)))
  }
}
