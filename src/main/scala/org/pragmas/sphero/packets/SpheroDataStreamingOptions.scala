/*
  ORIGINAL JAVA CODE:
  Copyright (C) 2014, Jean-Pierre de la Croix
  see the LICENSE file included with this software

  SCALA:
  Copyright (C) 2015, Massimo Maria Ghisalberti (zairik@gmail.com)

 */

package org.pragmas.sphero.packets

import org.pragmas.sphero.util.{ DataByteArray, SpheroLogger, SpheroMessages }
import language.implicitConversions

object SpheroDataStreamingOptions {

  private val NO_SF = 0.0
  private val DEFAULT_SF = 1.0
  private val ACCELEROMETER_AXIS_RAW_SF = 4.0e-3 // G (9.82 m/s^2)
  private val GYRO_AXIS_RAW_SF = 0.068 * Math.PI / 180.0 // rad/s
  private val IMU_ANGLE_FILTERED_SF = Math.PI / 180.0 // radians
  private val ACCELEROMETER_AXIS_FILTERED_SF = 1.0 / 4096.0 // G (9.82 m/s^2)
  private val GYRO_AXIS_FILTERED_SF = 0.1 * Math.PI / 180.0 // rad/s
  private val MOTOR_SF = 22.5 //centimeters
  private val QUARTERNION_SF = 1.0e-4
  private val ACCELONE_0_SF = 1.0

  object MASK extends Enumeration {

    val DISABLE = new MASK(0x00000000, NO_SF)

    val ACCELEROMETER_AXIS_X_RAW = MASK(0x80000000, ACCELEROMETER_AXIS_RAW_SF)
    val ACCELEROMETER_AXIS_Y_RAW = MASK(0x40000000, ACCELEROMETER_AXIS_RAW_SF)
    val ACCELEROMETER_AXIS_Z_RAW = MASK(0x20000000, ACCELEROMETER_AXIS_RAW_SF)

    val GYRO_AXIS_X_RAW = MASK(0x10000000, GYRO_AXIS_RAW_SF)
    val GYRO_AXIS_Y_RAW = MASK(0x08000000, GYRO_AXIS_RAW_SF)
    val GYRO_AXIS_Z_RAW = MASK(0x04000000, GYRO_AXIS_RAW_SF)

    //val RESERVED = new MASK(0x08000000, 0.0)
    //val RESERVED = new MASK(0x01000000, 0.0)
    //val RESERVED = new MASK(0x00800000, 0.0)

    val RIGHT_MOTOR_BACK_EMF_RAW = MASK(0x00400000, MOTOR_SF) //2.5 cm
    val LEFT_MOTOR_BACK_EMF_RAW = MASK(0x00200000, MOTOR_SF) //2.5 cm

    val RIGHT_MOTOR_PWN_RAW = MASK(0x00100000, MOTOR_SF)
    val LEFT_MOTOR_PWN_RAW = MASK(0x00080000, MOTOR_SF)

    val IMU_PITCH_ANGLE_FILTERED = MASK(0x00040000, IMU_ANGLE_FILTERED_SF)
    val IMU_ROLL_ANGLE_FILTERED = MASK(0x00020000, IMU_ANGLE_FILTERED_SF)
    val IMU_YAW_ANGLE_FILTERED = MASK(0x00010000, IMU_ANGLE_FILTERED_SF)

    val ACCELEROMETER_AXIS_X_FILTERED = MASK(0x00008000, ACCELEROMETER_AXIS_FILTERED_SF)
    val ACCELEROMETER_AXIS_Y_FILTERED = MASK(0x00004000, ACCELEROMETER_AXIS_FILTERED_SF)
    val ACCELEROMETER_AXIS_Z_FILTERED = MASK(0x00002000, ACCELEROMETER_AXIS_FILTERED_SF)

    val GYRO_AXIS_X_FILTERED = MASK(0x00001000, GYRO_AXIS_FILTERED_SF)
    val GYRO_AXIS_Y_FILTERED = MASK(0x00000800, GYRO_AXIS_FILTERED_SF)
    val GYRO_AXIS_Z_FILTERED = MASK(0x00000400, GYRO_AXIS_FILTERED_SF)

    val RIGHT_MOTOR_BACK_EMF_FILTERED = MASK(0x00000040, MOTOR_SF) //2.5 cm
    val LEFT_MOTOR_BACK_EMF_FILTERED = MASK(0x00000020, MOTOR_SF) //2.5 cm

    //val RESERVED = new MASK(0x00000200, 0.0)
    //val RESERVED = new MASK(0x00000100, 0.0)
    //val RESERVED = new MASK(0x00000080, 0.0)

    //val RESERVED1 = new MASK(0x00000010, 0.0)
    //val RESERVED2 = new MASK(0x00000008, 0.0)
    //val RESERVED3 = new MASK(0x00000004, 0.0)
    //val RESERVED4 = new MASK(0x00000002, 0.0)
    //val RESERVED5 = new MASK(0x00000001, 0.0)

    case class MASK(optionBitMask: Int = 0, scaleFactor: Double = 0.0) extends Val {
      //def getOptionBitMask(): Int = { optionBitMask }
      //def getScaleFactor(): Double = { scaleFactor }
    }

    implicit def convertValue(v: Value): MASK = v.asInstanceOf[MASK]
  }

  object MASK2 extends Enumeration {
    val QUARTERNION_Q0 = MASK2(0x80000000, QUARTERNION_SF)
    val QUARTERNION_Q1 = MASK2(0x40000000, QUARTERNION_SF)
    val QUARTERNION_Q2 = MASK2(0x20000000, QUARTERNION_SF)
    val QUARTERNION_Q3 = MASK2(0x10000000, QUARTERNION_SF)
    val ODOMETER_X = MASK2(0x08000000, DEFAULT_SF)
    val ODOMETER_Y = MASK2(0x04000000, DEFAULT_SF)
    val ACCELONE_0 = MASK2(0x02000000, ACCELONE_0_SF)
    val VELOCITY_X = MASK2(0x01000000, DEFAULT_SF)
    val VELOCITY_Y = MASK2(0x00800000, DEFAULT_SF)

    case class MASK2(optionBitMask: Int = 0, scaleFactor: Double = 0.0) extends Val {
      //def getOptionBitMask(): Int = { optionBitMask }
      //def getScaleFactor(): Double = { scaleFactor }
    }

    implicit def convertValue(v: Value): MASK2 = v.asInstanceOf[MASK2]
  }
}

class SpheroDataStreamingOptions(factor: Int, nSamples: Int, nPackets: Int) extends SpheroLogger {

  var optionsMask: Int = 0
  def getOptionsMask(): Int = { optionsMask }

  var optionsMask2: Int = 0
  def getOptionsMask2(): Int = { optionsMask }

  private var samplingRateFactor: Int = 1 // 400Hz
  private var samplesPerPacket: Int = 1 // 1 sample/packet
  private var packetsPerStream: Int = 1 // 1 packet/stream

  setSamplingRateFactor(factor)
  setSamplesPerPacket(nSamples)
  setPacketsPerStream(nPackets)

  def this() { this(1, 1, 1) }

  def setSamplingRateFactor(factor: Int): Unit = {
    if (samplingRateFactor > 0 && samplingRateFactor <= java.lang.Short.MAX_VALUE) {
      samplingRateFactor = factor
    } else {
      logger.error(SpheroMessages.ERROR_SAMPLING_RATE_FACTOR)
    }
  }

  def setSamplesPerPacket(nSamples: Int): Unit = {
    if (nSamples > 0 && nSamples <= java.lang.Short.MAX_VALUE) {
      samplesPerPacket = nSamples
    } else {
      logger.error(SpheroMessages.ERROR_FRAMES_PER_PACKETS)
    }
  }

  def setPacketsPerStream(nPackets: Int): Unit = {
    if (nPackets == 0) {
      packetsPerStream = nPackets
      logger.info(SpheroMessages.INFO_DATA_STREAM_CONTINUE)
    } else if (nPackets > 0 && nPackets <= 255) {
      packetsPerStream = nPackets
      logger.info(SpheroMessages.INFO_DATA_STREAM_ENDING, nPackets.toString)
    } else {
      logger.error(SpheroMessages.ERROR_PACKETS_ZERO_VALUE)
    }
  }

  def addOptions(optionBitMasks: SpheroDataStreamingOptions.MASK.MASK*): Unit = {
    optionBitMasks.foreach { option => optionsMask |= option.optionBitMask }
  }

  def addOptions2(optionBitMasks: SpheroDataStreamingOptions.MASK2.MASK2*): Unit = {
    optionBitMasks.foreach { option => optionsMask2 |= option.optionBitMask }
  }

  def toByteArray: Array[Byte] = {
    val array = new DataByteArray()
    array.writeTwoBytes(samplingRateFactor)
    array.writeTwoBytes(samplesPerPacket)
    array.writeFourBytes(optionsMask)
    array.writeOneByte(packetsPerStream)
    if (optionsMask2 != 0) { array.writeFourBytes(optionsMask2) }
    array.toByteArray
  }
}

