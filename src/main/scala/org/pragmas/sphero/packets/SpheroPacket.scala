/*
  ORIGINAL JAVA CODE:
  Copyright (C) 2014, Jean-Pierre de la Croix
  see the LICENSE file included with this software

  SCALA:
  Copyright (C) 2015, Massimo Maria Ghisalberti (zairik@gmail.com)

 */

package org.pragmas.sphero.packets

import language.implicitConversions
import org.pragmas.sphero.util.Utils

object SpheroPacket {

  object SOP extends Enumeration {
    val DEFAULT = SOP(0xFF)
    val ASYNC = SOP(0xFE)
    val NORESET = SOP(0xFD)
    val NORESET_ASYNC = SOP(0xFC)

    case class SOP(aByteCode: Int) extends Val {
      val byteCode: Byte = aByteCode.toByte
    }

    implicit def convertValue(v: Value): SOP = v.asInstanceOf[SOP]
  }

  object DID extends Enumeration {
    val CORE = DID(0x00)
    val BOOTLOADER = DID(0x01)
    val SPHERO = DID(0x02)

    case class DID(aByteCode: Int) extends Val {
      val byteCode: Byte = aByteCode.toByte
    }

    implicit def convertValue(v: Value): DID = v.asInstanceOf[DID]
  }

  object CID extends Enumeration {

    // CORE Commands
    val PING = CID(0x01)
    val VERSION = CID(0x02)
    val SET_BT_NAME = CID(0x10)
    val GET_BT_NAME = CID(0x11)
    val SET_AUTO_RECONNECT = CID(0x12)
    val GET_AUTO_RECONNECT = CID(0x13)
    val GET_PWR_STATE = CID(0x20)
    val SET_PWR_NOTIFY = CID(0x21)
    val SLEEP = CID(0x22)
    val INACTIVE_TIMER = CID(0x25)
    val GOTO_BL = CID(0x30)
    val RUN_L1_DIAGS = CID(0x40)
    val RUN_L2_DIAGS = CID(0x41)

    // Sphero Commands
    val SET_CAL = CID(0x01)
    val SET_STABILIZ = CID(0x02)
    val SET_ROTATION_RATE = CID(0x03)
    val REENABLE_DEMO = CID(0x06)
    val SELF_LEVEL = CID(0x09)
    val SET_DATA_STREAMING = CID(0x11)
    val SET_COLLISION_DET = CID(0x12)
    val LOCATOR = CID(0x13)
    val SET_ACCELERO = CID(0x14)
    val READ_LOCATOR = CID(0x15)
    val SET_RGB_LED = CID(0x20)
    val SET_BACK_LED = CID(0x21)
    val GET_RGB_LED = CID(0x22)
    val ROLL = CID(0x30)
    val BOOST = CID(0x31)
    val MOVE = CID(0x32)
    val SET_RAW_MOTORS = CID(0x33)
    val SET_MOTION_TO = CID(0x34)
    val SET_OPTIONS_FLAG = CID(0x35)
    val GET_OPTIONS_FLAG = CID(0x36)
    val SET_TEMP_OPTIONS_FLAG = CID(0x37)
    val GET_TEMP_OPTIONS_FLAG = CID(0x38)
    val RUN_MACRO = CID(0x50)
    val SAVE_TEMP_MACRO = CID(0x51)
    val SAVE_MACRO = CID(0x52)
    val INIT_MACRO_EXECUTIVE = CID(0x54)
    val ABORT_MACRO = CID(0x55)
    val MACRO_STATUS = CID(0x56)
    val SET_MACRO_PARAM = CID(0x57)
    val APPEND_TEMP_MACRO_CHUNK = CID(0x58)
    val ERASE_ORBBAS = CID(0x60)
    val APPEND_FRAG = CID(0x61)
    val EXEC_ORBBAS = CID(0x62)
    val ABORT_ORBBAS = CID(0x63)
    val ANSWER_INPUT = CID(0x64)

    //SPhero Subcommands
    val SET_STABILIZ_OFF = CID(0x00).byteCode
    val SET_STABILIZ_ON = CID(0x01).byteCode
    val SET_CAL_MAX = 359
    val SET_CAL_MIN = 0

    case class CID(aByteCode: Int) extends Val {
      val byteCode: Byte = aByteCode.toByte
    }

    implicit def convertValue(v: Value): CID = v.asInstanceOf[CID]
  }

  object RSP_CODE extends Enumeration {
    val OK = new RSP_CODE(0x00)
    class RSP_CODE(aByteCode: Int) extends Val {
      val byteCode: Byte = aByteCode.toByte
    }
    implicit def convertValue(v: Value): RSP_CODE = v.asInstanceOf[RSP_CODE]
  }

  object ID_CODE extends Enumeration {
    val SENSOR_DATA_STREAMING = new ID_CODE(0x03)
    class ID_CODE(aByteCode: Int) extends Val {
      var byteCode: Byte = aByteCode.toByte
    }
    implicit def convertValue(v: Value): ID_CODE = v.asInstanceOf[ID_CODE]
  }
}

class SpheroPacket {

  protected var internalByteArray: Array[Byte] = null
  var isAsynchronous: Boolean = false

  override def toString: String = {
    if (internalByteArray.length > 0) Utils.baToString(internalByteArray)
    else null
  }

  def toByteArray: Array[Byte] = {
    if (internalByteArray.length > 0) internalByteArray.clone()
    else null
  }

  protected def computeChecksum(byteArray: Array[Byte], length: Int): Byte = {
    var checksum = 0
    (2 until (length - 1)).foreach { i => checksum += byteArray(i) }
    (checksum ^ 0xFF.toByte).toByte
  }
}
