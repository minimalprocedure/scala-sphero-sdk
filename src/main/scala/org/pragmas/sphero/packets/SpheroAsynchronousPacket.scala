/*
  ORIGINAL JAVA CODE:
  Copyright (C) 2014, Jean-Pierre de la Croix
  see the LICENSE file included with this software

  SCALA:
  Copyright (C) 2015, Massimo Maria Ghisalberti (zairik@gmail.com)

 */

package org.pragmas.sphero.packets

//import java.util.HashMap
import scala.collection.mutable.HashMap

class SpheroAsynchronousPacket(byteArray: Array[Byte]) extends SpheroResponsePacket(byteArray) {

  isAsynchronous = true

  def parseDataWithOptions(options: SpheroDataStreamingOptions): HashMap[String, Double] = {
    val data = new HashMap[String, Double]()
    if (internalByteArray(2) == SpheroPacket.ID_CODE.SENSOR_DATA_STREAMING.byteCode) {
      val length = (internalByteArray(3) << 8) | internalByteArray(4)

      var index = 5
      var value = 0

      SpheroDataStreamingOptions.MASK.values.foreach { option =>
        if ((option.optionBitMask & options.getOptionsMask) != 0) {
          value = (internalByteArray(index) << 8) | internalByteArray(index + 1)
          value = if (((value >> 15) & 1) == 1) { -value } else { value }
          val scaled_value = value * option.scaleFactor
          //data.put(option.name(), scaled_value)
          data.put(option.toString, scaled_value)
          index += 2
        }
      }

      SpheroDataStreamingOptions.MASK2.values.foreach { option =>
        if ((option.optionBitMask & options.getOptionsMask2) != 0) {
          value = (internalByteArray(index) << 8) | internalByteArray(index + 1)
          value = if (((value >> 15) & 1) == 1) { -value } else { value }
          val scaled_value = value * option.scaleFactor
          //data.put(option.name(), scaled_value)
          data.put(option.toString, scaled_value)
          index += 2
        }
      }
    }
    data
  }
}
