name := "Sphero-scala-sdk"

version := "1.0"

scalaVersion := "2.11.7"

fork := true

outputStrategy := Some(StdoutOutput)

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-library" % "2.11.7",
  "org.scala-lang" % "scala-compiler" % "2.11.7",
  "org.scala-lang" % "scala-reflect" % "2.11.7",
  "org.scala-lang" % "scala-actors" % "2.11.7",
  "org.scala-lang.modules" % "scala-swing_2.11" % "1.0.1",
  "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.3",
  "org.scala-lang.modules" % "scala-parser-combinators_2.11" % "1.0.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0"
)



