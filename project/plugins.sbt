logLevel := Level.Warn

//resolvers += "Sonatype OSS Releases" at "https://oss.sonatype.org/service/local/staging/deploy/maven2"

addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.6.0")

addSbtPlugin("org.ensime" % "ensime-sbt" % "0.3.1")

addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "4.0.0")

addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.6.0")

addSbtPlugin("org.xerial.sbt" % "sbt-pack" % "0.7.7")

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.1") 

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.0.6")


